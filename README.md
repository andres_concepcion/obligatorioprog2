Un Banco desea contar con una aplicación Web que le permita manejar sus clientes y sus cuentas bancarias. Esta debe ser manejada por los administrativos del Banco, y debe contar también con una página web a la que puedan acceder los clientes para ver sus cuentas y saldos de cuentas. Para ello se debe manejar los usuarios: cliente y administrador.

La aplicación deberá encargarse del mantenimiento (ABM) de todas las entidades necesarias, esta funcionalidad estará disponible sólo para usuarios administradores.

De los clientes, se deberán mantener cédula, nombre, apellido, dirección, email, password (en el caso de estar registrado en la web). De las cuentas se deberán mantener un número auto numérico otorgado por el sistema mayor a 1000, moneda, saldo inicial y saldo. Las cuentas pueden ser del tipo Caja de Ahorro o Cuenta Corriente y las cajas de ahorro pueden ser comunes o a Plazo Fijo. Las Cajas de Ahorro a Plazo Fijo tienen una fecha de inicio, una duración en años y un porcentaje de interés. Las Cuentas Corrientes tiene un número de chequera.

Requerimientos
- La aplicación debe contar con listados de:
- La totalidad de las cuentas
- Cuentas por tipo
- Clientes y cuentas por cliente.
- Cada listado debe mostrar el detalle de la cuenta y su cliente asociado.