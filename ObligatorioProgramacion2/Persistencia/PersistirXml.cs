﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.IO;

namespace ObligatorioProgramacion2.Persistencia
{
    public class PersistirXml
    {
        protected string nombreArchivo;
        protected String nombreRaiz;
        protected string direccion = HttpContext.Current.Server.MapPath(@"~\Xml\");
        public void crearDocumento()
        {
            XDocument xDoc = new XDocument();
            xDoc.Declaration = new XDeclaration("1.0", "utf-8", "yes");
            xDoc.AddFirst(new XElement(nombreRaiz));
            guardarDocumento(xDoc);
        }
        public void guardarDocumento(XDocument pxDoc)
        {
            pxDoc.Save(direccion + nombreArchivo);
        }
        public XDocument cargarDocumento()
        {
            if (!File.Exists(direccion + nombreArchivo))
                crearDocumento();
            XDocument xDoc = XDocument.Load(direccion + nombreArchivo);
            return xDoc;
        }
    }
}