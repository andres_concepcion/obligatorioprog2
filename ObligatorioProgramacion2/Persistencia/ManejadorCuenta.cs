﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.IO;
using ObligatorioProgramacion2.Dominio;

namespace ObligatorioProgramacion2.Persistencia
{
    public class ManejadorCuenta : PersistirXml
    {
        private static ManejadorCuenta _instancia;
        public static ManejadorCuenta getInstancia()
        {
            if (_instancia == null)
            {
                _instancia = new ManejadorCuenta();
            }
            return _instancia;
        }
        private ManejadorCuenta()
        {
            nombreRaiz = "Cuenta";
            nombreArchivo = nombreRaiz + ".xml";
        }
        public void agregarCajaAhorro(CajaAhorro pCuenta)
        {
            XDocument xDoc = cargarDocumento();//cargo el documento
            XElement Cuentas = xDoc.Root;//obtengo elemento raiz
            XElement cuenta = new XElement("Cuenta");//creo elemento nuevo
            //agrego los atributos
            cuenta.SetAttributeValue("Tipo", "CajaAhorro");
            cuenta.SetAttributeValue("Numero", pCuenta.Numero.ToString());
            cuenta.SetAttributeValue("CiCliente", pCuenta.CiCliente.ToString());
            cuenta.SetAttributeValue("Moneda", pCuenta.MonedaCuenta.ToString());
            cuenta.SetAttributeValue("SaldoInicial", pCuenta.SaldoInicial.ToString());
            cuenta.SetAttributeValue("Saldo", pCuenta.Saldo.ToString());
            Cuentas.Add(cuenta);//agrego nuevo elemento a la raiz
            guardarDocumento(xDoc);//guardo cambios
        }
        public void agregarCajaAhorroPlazoFijo(CajaAhorroPlazoFijo pCuenta)
        {
            XDocument xDoc = cargarDocumento();//cargo el documento
            XElement Cuentas = xDoc.Root;//obtengo elemento raiz
            XElement cuenta = new XElement("Cuenta");//creo elemento nuevo
            //agrego los atributos
            cuenta.SetAttributeValue("Tipo", "CajaAhorroPlazoFijo");
            cuenta.SetAttributeValue("Numero", pCuenta.Numero.ToString());
            cuenta.SetAttributeValue("CiCliente", pCuenta.CiCliente.ToString());
            cuenta.SetAttributeValue("Moneda", pCuenta.MonedaCuenta.ToString());
            cuenta.SetAttributeValue("SaldoInicial", pCuenta.SaldoInicial.ToString());
            cuenta.SetAttributeValue("Saldo", pCuenta.Saldo.ToString());
            cuenta.SetAttributeValue("FechaDeInicio", pCuenta.FechaInicio.ToShortDateString());
            cuenta.SetAttributeValue("Duracion", pCuenta.Duracion.ToString());
            cuenta.SetAttributeValue("PorcentajeDeInteres", pCuenta.PorcentajeInteres.ToString());
            Cuentas.Add(cuenta);//agrego nuevo elemento a la raiz
            guardarDocumento(xDoc);//guardo cambios
        }
        public void agregarCuentaCorriente(CuentaCorriente pCuenta)
        {
            XDocument xDoc = cargarDocumento();//cargo el documento
            XElement Cuentas = xDoc.Root;//obtengo elemento raiz
            XElement cuenta = new XElement("Cuenta");//creo elemento nuevo
            //agrego los atributos
            cuenta.SetAttributeValue("Tipo", "CuentaCorriente");
            cuenta.SetAttributeValue("Numero", pCuenta.Numero.ToString());
            cuenta.SetAttributeValue("CiCliente", pCuenta.CiCliente.ToString());
            cuenta.SetAttributeValue("Moneda", pCuenta.MonedaCuenta.ToString());
            cuenta.SetAttributeValue("SaldoInicial", pCuenta.SaldoInicial.ToString());
            cuenta.SetAttributeValue("Saldo", pCuenta.Saldo.ToString());
            cuenta.SetAttributeValue("NroChequera", pCuenta.NroChequera.ToString());
            Cuentas.Add(cuenta);//agrego nuevo elemento a la raiz
            guardarDocumento(xDoc);//guardo cambios
        }
        public void updateSaldo(Cuenta pCuenta, decimal pMonto)
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            //Busco elemento Usuario por el numero
            XElement cuent = xDoc.Root.Elements().Single(a => (long)a.Attribute("Numero") == pCuenta.Numero);//sentencia linq
            //Modifico los atributos
            cuent.SetAttributeValue("Saldo", (pCuenta.Saldo + pMonto).ToString());
            guardarDocumento(xDoc);//guardo cambios
        }
        public void remove(long pNroCuenta)
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            foreach (XElement xml in xDoc.Root.Elements())//Busco elemento Usuario por el numero
            {
                if ((long)xml.Attribute("Numero") == pNroCuenta)
                {
                    xml.Remove();//elimino el nodo
                }
            }
            guardarDocumento(xDoc);//guardo cambios
        }
        public List<CajaAhorro> listarCajaAhorro()
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            List<CajaAhorro> lstaux = new List<CajaAhorro>();//lista auxiliar
            foreach (XElement xu in xDoc.Root.Elements())//recorro todos los elementos del archivo
            {
                if (xu.Attribute("Tipo").Value == "CajaAhorro")
                {
                    CajaAhorro cuenta = new CajaAhorro();//creo una caja de ahorro auxiliar
                    //cargo atributos en la caja de ahorro auxiliar
                    cuenta.Numero = long.Parse(xu.Attribute("Numero").Value);
                    cuenta.CiCliente = int.Parse(xu.Attribute("CiCliente").Value);
                    cuenta.MonedaCuenta = (Moneda)Enum.Parse(typeof(Moneda), xu.Attribute("Moneda").Value);
                    cuenta.SaldoInicial = decimal.Parse(xu.Attribute("SaldoInicial").Value);
                    cuenta.Saldo = decimal.Parse(xu.Attribute("Saldo").Value);
                    lstaux.Add(cuenta);//agrego caja de ahorro auxiliar a lista auxiliar
                }
            }
            return lstaux;//retorno lista
        }
        public List<CajaAhorroPlazoFijo> listarCajaAhorroPlazoFijo()
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            List<CajaAhorroPlazoFijo> lstaux = new List<CajaAhorroPlazoFijo>();//lista auxiliar
            foreach (XElement xu in xDoc.Root.Elements())//recorro todos los elementos del archivo
            {
                if (xu.Attribute("Tipo").Value == "CajaAhorroPlazoFijo")
                {
                    CajaAhorroPlazoFijo cuenta = new CajaAhorroPlazoFijo();//creo una caja de ahorro auxiliar
                    //cargo atributos en caja de ahorro auxiliar
                    cuenta.Numero = long.Parse(xu.Attribute("Numero").Value);
                    cuenta.CiCliente = int.Parse(xu.Attribute("CiCliente").Value);
                    cuenta.MonedaCuenta = (Moneda)Enum.Parse(typeof(Moneda), xu.Attribute("Moneda").Value);
                    cuenta.SaldoInicial = decimal.Parse(xu.Attribute("SaldoInicial").Value);
                    cuenta.Saldo = decimal.Parse(xu.Attribute("Saldo").Value);
                    cuenta.FechaInicio = DateTime.Parse(xu.Attribute("FechaDeInicio").Value);
                    cuenta.Duracion = int.Parse(xu.Attribute("Duracion").Value);
                    cuenta.PorcentajeInteres = decimal.Parse(xu.Attribute("PorcentajeDeInteres").Value);
                    lstaux.Add(cuenta);//agrego caja de ahorro auxiliar a lista auxiliar
                }
            }
            return lstaux;//retorno lista
        }
        public List<CuentaCorriente> listarCuentaCorriente()
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            List<CuentaCorriente> lstaux = new List<CuentaCorriente>();//lista auxiliar
            foreach (XElement xu in xDoc.Root.Elements())//recorro todos los elementos del archivo
            {
                if (xu.Attribute("Tipo").Value == "CuentaCorriente")
                {
                    CuentaCorriente cuenta = new CuentaCorriente();//creo una cuenta corriente auxiliar
                    //cargo atributos en cuenta corriente auxiliar
                    cuenta.Numero = long.Parse(xu.Attribute("Numero").Value);
                    cuenta.CiCliente = int.Parse(xu.Attribute("CiCliente").Value);
                    cuenta.MonedaCuenta = (Moneda)Enum.Parse(typeof(Moneda), xu.Attribute("Moneda").Value);
                    cuenta.SaldoInicial = decimal.Parse(xu.Attribute("SaldoInicial").Value);
                    cuenta.Saldo = decimal.Parse(xu.Attribute("Saldo").Value);
                    cuenta.NroChequera = int.Parse(xu.Attribute("NroChequera").Value);
                    lstaux.Add(cuenta);//agrego cuenta corriente auxiliar a lista auxiliar
                }
            }
            return lstaux;//retorno lista
        }
        public List<Cuenta> listarTodos()
        {
            List<Cuenta> lstaux = new List<Cuenta>();
            foreach (CajaAhorro cuenta in listarCajaAhorro())
            {
                lstaux.Add(cuenta);
            }
            foreach (CajaAhorroPlazoFijo cuenta in listarCajaAhorroPlazoFijo())
            {
                lstaux.Add(cuenta);
            }
            foreach (CuentaCorriente cuenta in listarCuentaCorriente())
            {
                lstaux.Add(cuenta);
            }
            return lstaux;//retorno lista
        }
    }
}