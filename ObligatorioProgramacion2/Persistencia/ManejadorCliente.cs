﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.IO;
using ObligatorioProgramacion2.Dominio;

namespace ObligatorioProgramacion2.Persistencia
{
    public class ManejadorCliente : PersistirXml
    {
        private static ManejadorCliente _instancia;
        public static ManejadorCliente getInstancia()
        {
            if (_instancia == null)
            {
                _instancia = new ManejadorCliente();
            }
            return _instancia;
        }
        private ManejadorCliente()
        {
            nombreRaiz = "Cliente";
            nombreArchivo = nombreRaiz + ".xml";
        }
        public void agregar(Cliente pCliente)
        {
            XDocument xDoc = cargarDocumento();//cargo el documento
            XElement Usuarios = xDoc.Root;//obtengo elemento raiz
            XElement cli = new XElement("Cliente");//creo elemento nuevo
            //agrego los atributos
            cli.SetAttributeValue("Ci", pCliente.Ci.ToString());
            cli.SetAttributeValue("Nombre", pCliente.Nombre.ToString());
            cli.SetAttributeValue("Apellido", pCliente.Apellido.ToString());
            cli.SetAttributeValue("Direccion", pCliente.Direccion.ToString());
            cli.SetAttributeValue("Email", pCliente.Email.ToString());
            cli.SetAttributeValue("Password", pCliente.Password.ToString());
            cli.SetAttributeValue("TipoDeUsuario",pCliente.TipoUsuario.ToString() );
            Usuarios.Add(cli);//agrego nuevo elemento a la raiz
            guardarDocumento(xDoc);//guardo cambios
        }
        public void update(Cliente pCliente)
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            //Busco elemento Usuario por la Ci
            XElement cli = xDoc.Root.Elements().Single(a => (int)a.Attribute("Ci") == pCliente.Ci);//sentencia linq
            //Modifico los atributos
            cli.SetAttributeValue("Ci", pCliente.Ci.ToString());
            cli.SetAttributeValue("Nombre", pCliente.Nombre.ToString());
            cli.SetAttributeValue("Apellido", pCliente.Apellido.ToString());
            cli.SetAttributeValue("Direccion", pCliente.Direccion.ToString());
            cli.SetAttributeValue("Email", pCliente.Email.ToString());
            cli.SetAttributeValue("Password", pCliente.Password.ToString());
            cli.SetAttributeValue("TipoDeUsuario", pCliente.TipoUsuario.ToString());
            guardarDocumento(xDoc);//guardo cambios
        }
        public void remove(int pCi)
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            foreach (XElement xml in xDoc.Root.Elements())//Busco elemento cliente por la Ci
            {
                if ((int)xml.Attribute("Ci") == pCi)
                {
                    xml.Remove();//elimino el nodo
                }
            }
            guardarDocumento(xDoc);//guardo cambios
        }
        public List<Cliente> listarTodos()
        {
            XDocument xDoc = cargarDocumento();//cargo documento
            List<Cliente> lstaux = new List<Cliente>();//lista auxiliar
            foreach (XElement xu in xDoc.Root.Elements())//recorro todos los elementos del archivo
            {
                Cliente cli = new Cliente();//creo un cliente auxiliar
                //cargo atributos en cliente auxiliar
                cli.Ci = int.Parse(xu.Attribute("Ci").Value);
                cli.Nombre = xu.Attribute("Nombre").Value;
                cli.Apellido = xu.Attribute("Apellido").Value;
                cli.Direccion = xu.Attribute("Direccion").Value;
                cli.Email = xu.Attribute("Email").Value;
                cli.Password = xu.Attribute("Password").Value;
                cli.TipoUsuario = (TipoUsuario)Enum.Parse(typeof(TipoUsuario), xu.Attribute("TipoDeUsuario").Value);
                cli.LstCuentas = new List<Cuenta>();
                lstaux.Add(cli);//agrego cliente auxiliar a lista auxiliar
            }
            return lstaux;//retorno lista
        }
    }
}