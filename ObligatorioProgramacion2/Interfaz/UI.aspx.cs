﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObligatorioProgramacion2.Dominio;

namespace ObligatorioProgramacion2.Interfaz
{
    public partial class UI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Usuario"] = null;
                Session["TipoUsuario"] = null;
                MultiView1.ActiveViewIndex = 0;
                Menu1.Enabled = false;
                Menu1.Visible = false;
                Menu2.Enabled = false;
                Menu2.Visible = false;
                Menu3.Enabled = false;
                Menu3.Visible = false;
                txtUsuario.Text = string.Empty;
                txtPassword.Text = string.Empty;
                txtCiRegistro.Text = string.Empty;
                txtContraseniaActual.Text = string.Empty;
                txtRepetirContrasenia.Text = string.Empty;
            }
        }
        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            MultiView1.ActiveViewIndex = int.Parse(Menu1.SelectedValue);
            txtCiABMCliente.Text = string.Empty;
            txtNombreABMCliente.Text = string.Empty;
            txtApellidoABMCliente.Text = string.Empty;
            txtDireccionABMCliente.Text = string.Empty;
            txtEmailABMCliente.Text = string.Empty;

            txtCiAltaCuenta.Text = string.Empty;
            txtNombreAltaCuenta.Text = string.Empty;
            txtSaldoInicial.Text = string.Empty;
            txtDuracion.Text = string.Empty;
            txtInteres.Text = string.Empty;

            txtCiListadoClientes.Text = string.Empty;
        }
        protected void Menu2_MenuItemClick(object sender, MenuEventArgs e)
        {
            MultiView1.ActiveViewIndex = int.Parse(Menu2.SelectedValue);
        }
        protected void Menu3_MenuItemClick(object sender, MenuEventArgs e)
        {
            Session["Usuario"] = null;
            Session["TipoUsuario"] = null;
            Menu1.Enabled = false;
            Menu1.Visible = false;
            Menu2.Enabled = false;
            Menu2.Visible = false;
            Menu3.Enabled = false;
            Menu3.Visible = false;
            txtUsuario.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtCiRegistro.Text = string.Empty;
            txtContraseniaActual.Text = string.Empty;
            txtRepetirContrasenia.Text = string.Empty;
            MultiView1.ActiveViewIndex = 0;
        }
        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtUsuario.Text));
            if (auxCliente != null)
            {
                if (txtPassword.Text == auxCliente.Password)
                {
                    Session["Usuario"] = auxCliente.Ci;
                    if (auxCliente.TipoUsuario.ToString() == "CLIENTE")
                    {
                        Session["TipoUsuario"] = "CLIENTE";
                        MultiView1.ActiveViewIndex = 6;
                        Menu2.Enabled = true;
                        Menu2.Visible = true;
                        Menu3.Enabled = true;
                        Menu3.Visible = true;
                        lstBoxCuentasClienteCliente.DataSource = Banco.getInstancia().listarCuentasPorCliente(auxCliente.Ci);
                        lstBoxCuentasClienteCliente.DataBind();
                    }
                    else if (auxCliente.TipoUsuario.ToString() == "ADMINISTRADOR")
                    {
                        Session["TipoUsuario"] = "ADMINISTRADOR";
                        MultiView1.ActiveViewIndex = 1;
                        Menu1.Enabled = true;
                        Menu1.Visible = true;
                        Menu3.Enabled = true;
                        Menu3.Visible = true;
                        lstBoxClientes.DataSource = Banco.getInstancia().cargarClientes();
                        lstBoxClientes.DataBind();
                    }
                }
                else if (txtPassword.Text != auxCliente.Password)
                    lblErrorLogin.Text = "La contrasenia no es correcta";
                else if (txtPassword.Text == "null")
                    lblErrorLogin.Text = "El cliente no se ha registrado en el sistema";
            }
            else if (auxCliente == null)
                lblErrorLogin.Text = "El cliente no existe";
        }
        protected void btnRegistrarse_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }
        protected void btnBuscarCiABMCliente_Click(object sender, EventArgs e)
        {
            Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtCiABMCliente.Text));
            if (auxCliente != null)
            {
                txtNombreABMCliente.Text = auxCliente.Nombre;
                txtApellidoABMCliente.Text = auxCliente.Apellido;
                txtDireccionABMCliente.Text = auxCliente.Direccion;
                txtEmailABMCliente.Text = auxCliente.Email;
                ddlTipoClienteABMCLiente.SelectedValue = auxCliente.TipoUsuario.ToString();
            }
            else
            {
                txtNombreABMCliente.Text = string.Empty;
                txtApellidoABMCliente.Text = string.Empty;
                txtDireccionABMCliente.Text = string.Empty;
                txtEmailABMCliente.Text = string.Empty;
                ddlTipoClienteABMCLiente.SelectedIndex = 0;
                lblVerifABMCliente.Text = "El cliente no existe";
            }
        }
        protected void btnAgregarCliente_Click(object sender, EventArgs e)
        {
            bool aux;
            try
            {
                if (txtCiABMCliente.Text != string.Empty && txtNombreABMCliente.Text != string.Empty && txtApellidoABMCliente.Text != string.Empty && txtDireccionABMCliente.Text != string.Empty && txtEmailABMCliente.Text != string.Empty)
                {
                    aux = Banco.getInstancia().altaCliente(int.Parse(txtCiABMCliente.Text), txtNombreABMCliente.Text, txtApellidoABMCliente.Text, txtDireccionABMCliente.Text, txtEmailABMCliente.Text, (TipoUsuario)Enum.Parse(typeof(TipoUsuario), ddlTipoClienteABMCLiente.SelectedValue.ToString()));
                    if (aux == false)
                        lblVerifABMCliente.Text = "El cliente ya existe";
                    else if (aux == true)
                        lblVerifABMCliente.Text = "Cliente agregado con exito";
                }
                else
                    lblVerifABMCliente.Text = "Ningun campo puede estar vacio";
            }
            catch (Exception ex)
            {
                lblVerifABMCliente.Text = ex.ToString();
            }
        }
        protected void btnModificarCliente_Click(object sender, EventArgs e)
        {
            bool aux = false;
            try
            {
                aux = Banco.getInstancia().modificarCliente(int.Parse(txtCiABMCliente.Text), txtNombreABMCliente.Text, txtApellidoABMCliente.Text, txtDireccionABMCliente.Text, txtEmailABMCliente.Text, (TipoUsuario)Enum.Parse(typeof(TipoUsuario), ddlTipoClienteABMCLiente.SelectedValue));
                if (aux == false)
                    lblVerifABMCliente.Text = "No se ha modificado el cliente";
                else if (aux == true)
                    lblVerifABMCliente.Text = "Cliente modificado con exito";
            }
            catch (Exception ex)
            {
                lblVerifABMCliente.Text = ex.ToString();
            }
        }
        protected void btnEliminarCliente_Click(object sender, EventArgs e)
        {
            bool aux;
            Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtCiABMCliente.Text));
            if (auxCliente != null)
            {
                aux = Banco.getInstancia().bajaCliente(auxCliente.Ci);
                if (aux == true)
                    lblVerifABMCliente.Text = "Cliente eliminado con exito";
                else if (aux == false)
                    lblVerifABMCliente.Text = "No se ha eliminado el cliente porque tiene cuentas asociadas";
            }
            else if (auxCliente == null)
                lblVerifABMCliente.Text = "El cliente no existe";
        }
        protected void btnRegistrarUsuario_Click(object sender, EventArgs e)
        {
            bool aux = false;
            if (txtContraseniaRegistro.Text == txtRepetirContrasenia.Text)
            {
                Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtCiRegistro.Text));
                if (auxCliente != null)
                {
                    aux = Banco.getInstancia().altaUsuario(auxCliente.Ci, txtContraseniaRegistro.Text);
                    lblVerifRegistroUsuario.Text = "Usuario registrado con exito";
                    btnRegistroInicioSesion.Visible = true;
                    btnRegistroInicioSesion.Enabled = true;
                }
                else if (auxCliente == null)
                    lblVerifRegistroUsuario.Text = "El cliente no existe";
            }
        }
        protected void btnRegistroInicioSesion_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
            btnRegistroInicioSesion.Visible = false;
            btnRegistroInicioSesion.Enabled = false;
        }
        protected void btnListadoCuentas_Click(object sender, EventArgs e)
        {
            if (ddlListadoCuenta.SelectedValue == "0")
            {
                lstBoxDetallesCuenta.Items.Clear();
                lstBoxDetallesCuenta.DataSource = Banco.getInstancia().listarTodas();
                lstBoxDetallesCuenta.DataBind();
            }
            else if (ddlListadoCuenta.SelectedValue == "CuentaCorriente")
            {
                lstBoxDetallesCuenta.Items.Clear();
                lstBoxDetallesCuenta.DataSource = Banco.getInstancia().listarCuentaCorriente();
                lstBoxDetallesCuenta.DataBind();
            }
            else if (ddlListadoCuenta.SelectedValue == "CajaAhorro")
            {
                lstBoxDetallesCuenta.Items.Clear();
                lstBoxDetallesCuenta.DataSource = Banco.getInstancia().listarCajaAhorro();
                lstBoxDetallesCuenta.DataBind();
            }
            else if (ddlListadoCuenta.SelectedValue == "CajaAhorroPlazoFijo")
            {
                lstBoxDetallesCuenta.Items.Clear();
                lstBoxDetallesCuenta.DataSource = Banco.getInstancia().listarCajaAhorroPlazoFijo();
                lstBoxDetallesCuenta.DataBind();
            }
        }
        protected void btnBuscarCiAltaCuenta_Click(object sender, EventArgs e)
        {
            txtNombreAltaCuenta.Text = string.Empty;
            if (txtCiAltaCuenta.Text != string.Empty)
            {
                Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtCiAltaCuenta.Text));
                if (auxCliente != null)
                    txtNombreAltaCuenta.Text = auxCliente.Nombre + " " + auxCliente.Apellido;
                else if (auxCliente == null)
                    txtNombreAltaCuenta.Text = "El cliente no existe";
            }
        }
        protected void btnTipoCuenta_Click(object sender, EventArgs e)
        {
            if (ddlTipoCuenta.SelectedValue == "CajaAhorroPlazoFijo")
            {
                lblMoneda.Visible = true;
                ddlMoneda.Visible = true;
                ddlMoneda.Enabled = true;

                lblSaldo.Visible = true;
                txtSaldoInicial.Visible = true;
                txtSaldoInicial.Enabled = true;

                lblDuracion.Visible = true;
                txtDuracion.Visible = true;
                txtDuracion.Enabled = true;

                lblInteres.Visible = true;
                txtInteres.Visible = true;
                txtInteres.Enabled = true;

                lblPorcentaje.Visible = true;
            }
            else
            {
                lblMoneda.Visible = true;
                ddlMoneda.Visible = true;
                ddlMoneda.Enabled = true;
                lblSaldo.Visible = true;
                txtSaldoInicial.Visible = true;
                txtSaldoInicial.Enabled = true;

                lblDuracion.Visible = false;
                txtDuracion.Visible = false;
                txtDuracion.Enabled = false;

                lblInteres.Visible = false;
                txtInteres.Visible = false;
                txtInteres.Enabled = false;

                lblPorcentaje.Visible = false;
            }
        }
        protected void btnAgregarAltaCuenta_Click(object sender, EventArgs e)
        {
            if (txtCiAltaCuenta.Text != string.Empty)
            {
                Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtCiAltaCuenta.Text));
                if (auxCliente != null)
                {
                    if (ddlTipoCuenta.SelectedValue == "CuentaCorriente" && txtSaldoInicial.Text != string.Empty)
                    {
                        Banco.getInstancia().altaCuentaCorriente(auxCliente.Ci, (Moneda)Enum.Parse(typeof(Moneda), ddlMoneda.SelectedValue), decimal.Parse(txtSaldoInicial.Text));
                        lblVerifAltaCuenta.Text = "Cuenta creada con exito";
                        Banco.getInstancia().cargarClientes();
                    }
                    else if (ddlTipoCuenta.SelectedValue == "CajaAhorro" && txtSaldoInicial.Text != string.Empty)
                    {
                        Banco.getInstancia().altaCajaDeAhorro(auxCliente.Ci, (Moneda)Enum.Parse(typeof(Moneda), ddlMoneda.SelectedValue), decimal.Parse(txtSaldoInicial.Text));
                        lblVerifAltaCuenta.Text = "Cuenta creada con exito";
                        Banco.getInstancia().cargarClientes();
                    }
                    else if (ddlTipoCuenta.SelectedValue == "CajaAhorroPlazoFijo" && txtSaldoInicial.Text != string.Empty && txtDuracion.Text != string.Empty && txtInteres.Text != string.Empty)
                    {
                        Banco.getInstancia().altaCajaDeAhorroPlazoFijo(auxCliente.Ci, (Moneda)Enum.Parse(typeof(Moneda), ddlMoneda.SelectedValue), decimal.Parse(txtSaldoInicial.Text), int.Parse(txtDuracion.Text), decimal.Parse(txtInteres.Text));
                        lblVerifAltaCuenta.Text = "Cuenta creada con exito";
                        Banco.getInstancia().cargarClientes();
                    }
                    else
                        lblVerifAltaCuenta.Text = "La cuenta no fue creada porque hay algun campo vacio";
                }
                else if (auxCliente == null)
                    lblVerifAltaCuenta.Text = "La cuenta no fue creada porque el cliente no existe";
            }
        }
        protected void btnBuscarListadoClientes_Click(object sender, EventArgs e)
        {
            if (txtCiListadoClientes.Text != string.Empty)
            {
                Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(int.Parse(txtCiListadoClientes.Text));
                if (auxCliente != null)
                {
                    lstBoxClientes.Items.Clear();
                    lstBoxClientes.Items.Add(auxCliente.ToString());
                    lstBoxCuentasCliente.Items.Clear();
                    lstBoxCuentasCliente.DataSource = Banco.getInstancia().listarCuentasPorCliente(auxCliente.Ci);
                    lstBoxCuentasCliente.DataBind();
                }
                else if (auxCliente == null)
                    lstBoxClientes.Items.Add("El cliente no existe");
            }
        }
        protected void btnListarCuentas_Click(object sender, EventArgs e)
        {
            lstBoxCuentasCliente.Items.Clear();
            List<Cliente> lstAux = Banco.getInstancia().cargarClientes();
            int aux = lstBoxClientes.SelectedIndex;
            lstBoxCuentasCliente.DataSource = Banco.getInstancia().listarCuentasPorCliente(lstAux[aux].Ci);
            lstBoxCuentasCliente.DataBind();
        }
        protected void btnEliminarCuenta_Click(object sender, EventArgs e)
        {
            int aux = lstBoxClientes.SelectedIndex;
            List<Cliente> lstAuxClientes = Banco.getInstancia().cargarClientes();
            Cliente auxCliente = lstAuxClientes[aux];
            int auxCuenta = lstBoxCuentasCliente.SelectedIndex;
            long auxNroCuenta = auxCliente.LstCuentas[auxCuenta].Numero;
            auxCliente.LstCuentas.RemoveAt(auxCuenta);
            Banco.getInstancia().bajaCuenta(auxNroCuenta);
            lstBoxCuentasCliente.Items.Remove(lstBoxCuentasCliente.SelectedItem);
        }
        protected void btnRefrescarClientes_Click(object sender, EventArgs e)
        {
            lstBoxCuentasCliente.Items.Clear();
            lstBoxClientes.DataSource = Banco.getInstancia().cargarClientes();
            lstBoxClientes.DataBind();
        }
        protected void btnCambiarContrasenia_Click(object sender, EventArgs e)
        {
            int aux = int.Parse(Session["Usuario"].ToString());
            Cliente auxCliente = Banco.getInstancia().buscarClienteXCi(aux);
            if (txtContraseniaActual.Text == auxCliente.Password && txtNuevaContrasenia == txtRepetirContraseniaCambio)
                Banco.getInstancia().cambiarContrasenia(auxCliente.Ci, txtNuevaContrasenia.Text);
        }
    }
}