﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UI.aspx.cs" Inherits="ObligatorioProgramacion2.Interfaz.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h1 style="width: 360px">NOMBRE APLICACION</h1>
        <br />
        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" OnMenuItemClick="Menu1_MenuItemClick">
            <Items>
                <asp:MenuItem Text="ABM Clientes" Value="1"></asp:MenuItem>
                <asp:MenuItem Text="ListadoCuentas" Value="3"></asp:MenuItem>
                <asp:MenuItem Text="AltaCuenta" Value="4"></asp:MenuItem>
                <asp:MenuItem Text="ListadoClientes" Value="5"></asp:MenuItem>
                <asp:MenuItem Text="Cambiar Contraseña" Value="8"></asp:MenuItem>
            </Items>
        </asp:Menu>
        <asp:Menu ID="Menu2" runat="server" Orientation="Horizontal" OnMenuItemClick="Menu2_MenuItemClick">
            <Items>
                <asp:MenuItem Text="Ver Cuentas" Value="7"></asp:MenuItem>
                <asp:MenuItem Text="Cambiar Contraseña" Value="8"></asp:MenuItem>
            </Items>
        </asp:Menu>
        <asp:Menu ID="Menu3" runat="server" OnMenuItemClick="Menu3_MenuItemClick">
            <Items>
                <asp:MenuItem Text="Salir" Value="Salir"></asp:MenuItem>
            </Items>
        </asp:Menu>
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="Login" runat="server">
                <asp:Label ID="lblUsuario" runat="server" Text="C.I."></asp:Label>
                <asp:TextBox ID="txtUsuario" runat="server" TextMode="Number" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValCiLogin" runat="server" ControlToValidate="txtUsuario" Display="Dynamic" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <asp:Label ID="lblErrorLogin" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValPassLogin" runat="server" ControlToValidate="txtPassword" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <br />
                <asp:Button ID="btnEntrar" runat="server" Text="ENTRAR" OnClick="btnEntrar_Click" />
                <asp:Button ID="btnRegistrarse" runat="server" Text="REGISTRARSE" OnClick="btnRegistrarse_Click" />
                <br />
            </asp:View>
            <asp:View ID="ABMCliente" runat="server">
                <asp:Label ID="lblCi" runat="server" Text="Ci"></asp:Label>
                <asp:TextBox ID="txtCiABMCliente" runat="server" TextMode="Number"></asp:TextBox>
                <asp:Button ID="btnBuscarCiABMCliente" runat="server" Text="Buscar" OnClick="btnBuscarCiABMCliente_Click" />
                <asp:RequiredFieldValidator ID="reqValCiABMCliente" runat="server" ControlToValidate="txtCiABMCliente" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
                <asp:TextBox ID="txtNombreABMCliente" runat="server"></asp:TextBox>
                <br />
                <asp:Label ID="lblApellido" runat="server" Text="Apellido"></asp:Label>
                <asp:TextBox ID="txtApellidoABMCliente" runat="server"></asp:TextBox>
                <br />
                <asp:Label ID="lblDireccion" runat="server" Text="Direccion"></asp:Label>
                <asp:TextBox ID="txtDireccionABMCliente" runat="server"></asp:TextBox>
                <br />
                <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                <asp:TextBox ID="txtEmailABMCliente" runat="server"></asp:TextBox>
                <br />
                <asp:Label ID="lblTipoUsuario" runat="server" Text="Tipo de Usuario"></asp:Label>
                <asp:DropDownList ID="ddlTipoClienteABMCLiente" runat="server">
                    <asp:ListItem Value="CLIENTE">Cliente</asp:ListItem>
                    <asp:ListItem Value="ADMINISTRADOR">Administrador</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                <asp:Button ID="btnAgregarCliente" runat="server" Text="Agregar" OnClick="btnAgregarCliente_Click" />
                &nbsp;
                <asp:Button ID="btnModificarCliente" runat="server" Text="Modificar" OnClick="btnModificarCliente_Click" />
                &nbsp;
                <asp:Button ID="btnEliminarCliente" runat="server" Text="Eliminar" OnClick="btnEliminarCliente_Click" />
                &nbsp;
                <asp:Label ID="lblVerifABMCliente" runat="server"></asp:Label>
                <br />
            </asp:View>
            <asp:View ID="Registro" runat="server">
                <asp:Label ID="lblCIUsuario" runat="server" Text="CI"></asp:Label>
                <asp:TextBox ID="txtCiRegistro" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValCiRegistro" runat="server" ControlToValidate="txtCiRegistro" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblContraseniaUsuario" runat="server" Text="Contraseña"></asp:Label>
                <asp:TextBox ID="txtContraseniaRegistro" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValContraseniaRegistro" runat="server" ControlToValidate="txtContraseniaRegistro" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblRepetirContraseniaRegistro" runat="server" Text="Repetir Contraseña"></asp:Label>
                <asp:TextBox ID="txtRepetirContrasenia" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValRepetirContraseniaRegistro" runat="server" ControlToValidate="txtRepetirContrasenia" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <br />
                <asp:Button ID="btnRegistrarUsuario" runat="server" Text="Registrarse" OnClick="btnRegistrarUsuario_Click" />
                <asp:Label ID="lblVerifRegistroUsuario" runat="server"></asp:Label>
                <asp:Button ID="btnRegistroInicioSesion" runat="server" Enabled="False" OnClick="btnRegistroInicioSesion_Click" Text="Inicio sesion" Visible="False" />
                <br />
            </asp:View>
            <asp:View ID="ListadosCuentas" runat="server">
                Listados de Cuentas<br />
                <br />
                <asp:Label ID="lblTipoCuentaListadoCuenta" runat="server" Text="Tipo de Cuenta"></asp:Label>
&nbsp;<asp:DropDownList ID="ddlListadoCuenta" runat="server" >
                    <asp:ListItem Value="0">Todas</asp:ListItem>
                    <asp:ListItem Value="CuentaCorriente">Cuenta Corriente</asp:ListItem>
                    <asp:ListItem Value="CajaAhorro">Caja de Ahorro </asp:ListItem>
                    <asp:ListItem Value="CajaAhorroPlazoFijo">Caja de Ahorro a Plazo Fijo</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnListadoCuentas" runat="server" OnClick="btnListadoCuentas_Click" Text="LISTAR" />
                <br />
                <br />
                <asp:Label ID="lblCuentasListadoCuentas" runat="server" Text="Cuentas"></asp:Label>
                <br />
                <asp:ListBox ID="lstBoxDetallesCuenta" runat="server" Height="180px" Width="690px"></asp:ListBox>
                <br />
            </asp:View>
            <asp:View ID="AltaCuenta" runat="server">
                Alta de Cuenta<br />
                <br />
                <asp:Label ID="Label1" runat="server" Text="Buscar cliente por C.I."></asp:Label>
                <asp:TextBox ID="txtCiAltaCuenta" runat="server" TextMode="Number"></asp:TextBox>
                <asp:Button ID="btnBuscarCiAltaCuenta" runat="server" Text="Buscar" OnClick="btnBuscarCiAltaCuenta_Click" />
                <br />
                <asp:Label ID="Label2" runat="server" Text="Nombre"></asp:Label>
                <asp:TextBox ID="txtNombreAltaCuenta" runat="server"></asp:TextBox>
                <br />
                <asp:Label ID="lblTipoCuenta" runat="server" Text="Tipo de Cuenta"></asp:Label>
                <asp:DropDownList ID="ddlTipoCuenta" runat="server">
                    <asp:ListItem Enabled="False">Seleccione</asp:ListItem>
                    <asp:ListItem Value="CuentaCorriente">Cuenta Corriente</asp:ListItem>
                    <asp:ListItem Value="CajaAhorro">Caja de Ahorro</asp:ListItem>
                    <asp:ListItem Value="CajaAhorroPlazoFijo">Caja de Ahorro a Plazo Fijo</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnTipoCuenta" runat="server" OnClick="btnTipoCuenta_Click" Text="Elegir" />
                <br />
                <asp:Label ID="lblMoneda" runat="server" Text="Moneda" Visible="False"></asp:Label>
                <asp:DropDownList ID="ddlMoneda" runat="server" Enabled="False" Visible="False">
                    <asp:ListItem Value="PESOS">Pesos</asp:ListItem>
                    <asp:ListItem Value="DOLARES">Dolares</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:Label ID="lblSaldo" runat="server" Text="Saldo Inicial" Visible="False"></asp:Label>
                <asp:TextBox ID="txtSaldoInicial" runat="server" Enabled="False" Visible="False" TextMode="Number"></asp:TextBox>
                <br />
                <asp:Label ID="lblDuracion" runat="server" Text="Duracion" Visible="False"></asp:Label>
                <asp:TextBox ID="txtDuracion" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                <br />
                <asp:Label ID="lblInteres" runat="server" Text="Taza Interes" Visible="False"></asp:Label>
                <asp:TextBox ID="txtInteres" runat="server" Enabled="False" Visible="False" TextMode="Number" Width="37px"></asp:TextBox>
                <asp:Label ID="lblPorcentaje" runat="server" Text="%" Visible="False"></asp:Label>
                <br />
                <asp:Button ID="btnAgregarAltaCuenta" runat="server" Text="Agregar" OnClick="btnAgregarAltaCuenta_Click" />
                <asp:Label ID="lblVerifAltaCuenta" runat="server"></asp:Label>
                <br />
            </asp:View>
            <asp:View ID="ListadoClientes" runat="server">
                <asp:Label ID="lblListados" runat="server" Text="Listados"></asp:Label>
                <br />
                <br />
                <asp:Label ID="lblBuscarCiListaoClientes" runat="server" Text="Buscar Cliente por CI"></asp:Label>
                <asp:TextBox ID="txtCiListadoClientes" runat="server" TextMode="Number"></asp:TextBox>
                <asp:Button ID="btnBuscarListadoClientes" runat="server" Text="Buscar" OnClick="btnBuscarListadoClientes_Click" />
                <br />
                <br />
                <asp:Label ID="lblCliente" runat="server" Text="Clientes"></asp:Label>
                <asp:Button ID="btnRefrescarClientes" runat="server" OnClick="btnRefrescarClientes_Click" Text="Refrescar" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <asp:ListBox ID="lstBoxClientes" runat="server" Height="138px" Width="840px"></asp:ListBox>
                &nbsp;&nbsp;&nbsp;<br />
                <br />
                <asp:Button ID="btnListarCuentas" runat="server" Text="=&gt;" OnClick="btnListarCuentas_Click"/>
                &nbsp;&nbsp;<br />
                <br />
                <asp:Label ID="lblCuentas" runat="server" Text="Cuentas"></asp:Label>
                <br />
                <asp:ListBox ID="lstBoxCuentasCliente" runat="server" Height="140px" Width="840px"></asp:ListBox>
                <asp:Button ID="btnEliminarCuenta" runat="server" Text="Eliminar" OnClick="btnEliminarCuenta_Click" />
                <br />
            </asp:View>
            <asp:View ID="PaginaCliente" runat="server">
                <br />
                <br />
                <h1>
                    <asp:Label ID="Label9" runat="server" Text="BIENVENIDO"></asp:Label>
                </h1>
                <br />
                <br />
            </asp:View>
            <asp:View ID="CuentasCliente" runat="server">
                <asp:Label ID="lblCuentasCuentasCliente" runat="server" Text="Cuentas"></asp:Label>
                <br />
                <asp:ListBox ID="lstBoxCuentasClienteCliente" runat="server" Width="1078px"></asp:ListBox>
                <br />
            </asp:View>
            <asp:View ID="CambiarContrasenia" runat="server">
                <asp:Label ID="lblContraseniaCambiarContrasenia" runat="server" Text="Contrasenia Actual"></asp:Label>
                <asp:TextBox ID="txtContraseniaActual" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValContraseniaActualCambiarContrasenia" runat="server" ControlToValidate="txtContraseniaActual" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblNuevaContrasenia" runat="server" Text="Nueva Contrasenia"></asp:Label>
                <asp:TextBox ID="txtNuevaContrasenia" runat="server" TextMode="Password" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValNuevaContraseniaCambiarContrasenia" runat="server" ControlToValidate="txtNuevaContrasenia" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblRepetirContrasenia" runat="server" Text="Repetir Contrasenia"></asp:Label>
                <asp:TextBox ID="txtRepetirContraseniaCambio" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqValRepetirContraseniaCambioContrasenia" runat="server" ControlToValidate="txtRepetirContraseniaCambio" ErrorMessage="El campo no puede estar vacio"></asp:RequiredFieldValidator>
                <br />
                <br />
                <asp:Button ID="btnCambiarContrasenia" runat="server" Text="Cambiar" OnClick="btnCambiarContrasenia_Click" />
                <asp:Label ID="lblErrorCambioContrasenia" runat="server"></asp:Label>
                <br />
            </asp:View>
        </asp:MultiView>
        <br />
        <br />
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
