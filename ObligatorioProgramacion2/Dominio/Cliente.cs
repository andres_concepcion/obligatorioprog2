﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObligatorioProgramacion2.Dominio
{
    public enum TipoUsuario
    {
        CLIENTE = 0,
        ADMINISTRADOR = 1
    }
    public class Cliente
    {
        #region //Atributos
        private int _ci;
        private string _nombre;
        private string _apellido;
        private string _direccion;
        private string _email;
        private string _password;
        private TipoUsuario _tipoUsuario;
        private List<Cuenta> _lstCuentas;
        #endregion
        #region //Propiedades
        public int Ci
        {
            get { return _ci; }
            set { _ci = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public string Apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public TipoUsuario TipoUsuario
        {
            get { return _tipoUsuario; }
            set { _tipoUsuario = value; }
        }
        public List<Cuenta> LstCuentas
        {
            get { return _lstCuentas; }
            set { _lstCuentas = value; }
        }
        #endregion
        #region //Metodos
        public Cliente() { }
        public Cliente(int pCi, string pNombre, string pApellido, string pDireccion, string pEmail, TipoUsuario pTipoUsuario)
        {
            _ci = pCi;
            _nombre = pNombre;
            _apellido = pApellido;
            _direccion = pDireccion;
            _email = pEmail;
            _password = "null";
            _tipoUsuario = pTipoUsuario;
            _lstCuentas = new List<Cuenta>();
        }
        public override string ToString()
        {
            return "Cedula: " + _ci + " Nombre: " + _nombre + " Apellido: " + _apellido + " Direccion: " + _direccion + " Email: " + _email;
        }
        #endregion
    }
}