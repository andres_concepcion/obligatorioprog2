﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObligatorioProgramacion2.Dominio
{
    public class CuentaCorriente : Cuenta
    {
        #region //Atributos
        private int _nroChequera;
        #endregion
        #region //Propiedades
        public int NroChequera
        {
            get { return _nroChequera; }
            set { _nroChequera = value; }
        }
        #endregion
        #region //Metodos
        public CuentaCorriente() { }
        public CuentaCorriente(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial)
            : base(pCiCliente, pMoneda, pSaldoInicial)
        {
            SaldoInicial = pSaldoInicial;
            Saldo = pSaldoInicial;
        }
        public override string ToString()
        {
            return "CuentaCorriente " + base.ToString() + " Nro de chequera: " + _nroChequera;
        }
        #endregion
    }
}