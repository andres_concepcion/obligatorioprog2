﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObligatorioProgramacion2.Dominio
{
    public class CajaAhorro : Cuenta
    {
        #region //Metodos
        public CajaAhorro() { }
        public CajaAhorro(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial)
            : base(pCiCliente, pMoneda, pSaldoInicial)
        {
            CiCliente = pCiCliente;
            MonedaCuenta = pMoneda;
            SaldoInicial = pSaldoInicial;
            Saldo = pSaldoInicial;
        }
        public override string ToString()
        {
            return "CajaAhorro " + base.ToString();
        }
        #endregion
    }
}