﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObligatorioProgramacion2.Dominio
{
    public enum Moneda
    {
        PESOS = 0,
        DOLARES = 1
    }
    public class Cuenta
    {
        #region //Atributos
        private long _numero;
        private int _ciCliente;
        private Moneda _monedaCuenta;
        private decimal _saldoInicial;
        private decimal _saldo;
        #endregion
        #region//Propiedades
        public long Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }
        public int CiCliente
        {
            get { return _ciCliente; }
            set { _ciCliente = value; }
        }
        public Moneda MonedaCuenta
        {
            get { return _monedaCuenta; }
            set { _monedaCuenta = value; }
        }
        public decimal SaldoInicial
        {
            get { return _saldoInicial; }
            set { _saldoInicial = value; }
        }
        public decimal Saldo
        {
            get { return _saldo; }
            set { _saldo = value; }
        }
        #endregion
        #region //Metodos
        public Cuenta() { }
        public Cuenta(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial)
        {
            _ciCliente = pCiCliente;
            _monedaCuenta = pMoneda;
            _saldoInicial = pSaldoInicial;
            _saldo = pSaldoInicial;
        }
        public override string ToString()
        {
            return "Nro de cuenta: " + _numero + " Ci titular: " + _ciCliente + " Moneda: " + _monedaCuenta + " Saldo: " + _saldo;
        }
        #endregion
    }
}