﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObligatorioProgramacion2.Persistencia;

namespace ObligatorioProgramacion2.Dominio
{
    public class Banco
    {
        #region //Singleton
        private static Banco Instancia;
        private Banco()
        {
            _lstCuentas = listarTodas();
            _lstClientes = cargarClientes();
        }
        public static Banco getInstancia()
        {
            if (Instancia == null)
            {
                Instancia = new Banco();
            }
            return Instancia;
        }
        #endregion
        #region //Atributos
        private List<Cliente> _lstClientes = new List<Cliente>();
        private List<Cuenta> _lstCuentas = new List<Cuenta>();
        #endregion
        #region //Atributos
        public List<Cliente> LstUsuarios
        {
            get { return _lstClientes; }
            set { _lstClientes = value; }
        }
        public List<Cuenta> LstCuentas
        {
            get { return _lstCuentas; }
            set { _lstCuentas = value; }
        }
        #endregion
        #region //Metodos
        #region //Usuario
        public List<Cliente> cargarClientes()
        {
            List<Cliente> lstAux = ManejadorCliente.getInstancia().listarTodos();
            foreach (Cliente auxCliente in lstAux)
            {
                foreach (Cuenta auxCuenta in _lstCuentas)
                {
                    if (auxCliente.Ci == auxCuenta.CiCliente)
                        auxCliente.LstCuentas.Add(auxCuenta);
                }
            }
            return lstAux;
        }
        public bool altaCliente(int pCi, string pNombre, string pApellido, string pDireccion, string pEmail, TipoUsuario pTipoUsuario)
        {
            bool verificador = false;
            if (buscarClienteXCi(pCi) == null)
            {
                Cliente auxCliente = new Cliente(pCi, pNombre, pApellido, pDireccion, pEmail, pTipoUsuario);
                ManejadorCliente.getInstancia().agregar(auxCliente);
                verificador = true;
            }
            return verificador;
        }
        public bool altaUsuario(int pCi, string pPass)
        {
            bool verificador = false;
            Cliente auxCliente = buscarClienteXCi(pCi);
            if (auxCliente.Password == "null")
            {
                auxCliente.Password = pPass;
                ManejadorCliente.getInstancia().update(auxCliente);
                verificador = true;
            }
            return verificador;
        }
        public void cambiarContrasenia(int pCi, string pPass)
        {
            Cliente auxCli = buscarClienteXCi(pCi);
            auxCli.Password = pPass;
            ManejadorCliente.getInstancia().update(auxCli);
        }
        public bool bajaCliente(int pCi)
        {
            bool verificador = false;
            Cliente auxCliente = buscarClienteXCi(pCi);
            if (auxCliente != null && auxCliente.LstCuentas.Count == 0)
            {
                ManejadorCliente.getInstancia().remove(auxCliente.Ci);
                verificador = true;
            }
            return verificador;
        }
        public bool modificarCliente(int pCi, string pNombre, string pApellido, string pDireccion, string pEmail, TipoUsuario pTipoUsuario)
        {
            bool verificador = false;
            Cliente auxCliente = buscarClienteXCi(pCi);
            if (auxCliente != null)
            {
                auxCliente.Ci = pCi;
                auxCliente.Nombre = pNombre;
                auxCliente.Apellido = pApellido;
                auxCliente.Direccion = pDireccion;
                auxCliente.Email = pEmail;
                auxCliente.TipoUsuario = pTipoUsuario;
                ManejadorCliente.getInstancia().update(auxCliente);
                verificador = true;
            }
            return verificador;
        }
        public Cliente buscarClienteXCi(int pCi)
        {
            Cliente auxCliente = null;
            _lstClientes = cargarClientes();
            foreach (Cliente cli in _lstClientes)
            {
                if (cli.Ci == pCi)
                {
                    auxCliente = cli;
                    break;
                }
            }
            return auxCliente;
        }
        public List<Cuenta> listarCuentasPorCliente(int pCi)
        {
            List<Cuenta> lstAux = new List<Cuenta>();
            Cliente auxCliente = buscarClienteXCi(pCi);
            foreach (Cuenta cu in auxCliente.LstCuentas)
            { lstAux.Add(cu); }
            return lstAux;
        }
        #endregion
        #region //Cuenta
        public void altaCuentaCorriente(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial)
        {
            long numeroCuenta = _lstCuentas.Count();
            numeroCuenta += 1000;
            CuentaCorriente auxCuenta = new CuentaCorriente(pCiCliente, pMoneda, pSaldoInicial);
            auxCuenta.Numero = numeroCuenta;
            ManejadorCuenta.getInstancia().agregarCuentaCorriente(auxCuenta);
            _lstCuentas = listarTodas();
        }
        public void altaCajaDeAhorro(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial)
        {
            long numeroCuenta = _lstCuentas.Count();
            numeroCuenta += 1000;
            CajaAhorro auxCuenta = new CajaAhorro(pCiCliente, pMoneda, pSaldoInicial);
            auxCuenta.Numero = numeroCuenta;
            ManejadorCuenta.getInstancia().agregarCajaAhorro(auxCuenta);
            _lstCuentas = listarTodas();
        }
        public void altaCajaDeAhorroPlazoFijo(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial, int pDuracion, decimal pPorcentaje)
        {
            long numeroCuenta = _lstCuentas.Count();
            numeroCuenta += 1000;
            CajaAhorroPlazoFijo auxCuenta = new CajaAhorroPlazoFijo(pCiCliente, pMoneda, pSaldoInicial, DateTime.Now, pDuracion, pPorcentaje);
            auxCuenta.Numero = numeroCuenta;
            ManejadorCuenta.getInstancia().agregarCajaAhorroPlazoFijo(auxCuenta);
            _lstCuentas = listarTodas();
        }
        public void bajaCuenta(long pNumero)
        {
            foreach (Cuenta cu in _lstCuentas)
            {
                if (cu.Numero == pNumero)
                {
                    _lstCuentas.Remove(cu);
                    ManejadorCuenta.getInstancia().remove(pNumero);
                    break;
                }
            }
            _lstCuentas = listarTodas();
        }
        public List<CajaAhorro> listarCajaAhorro()
        { return ManejadorCuenta.getInstancia().listarCajaAhorro(); }
        public List<CajaAhorroPlazoFijo> listarCajaAhorroPlazoFijo()
        { return ManejadorCuenta.getInstancia().listarCajaAhorroPlazoFijo(); }
        public List<CuentaCorriente> listarCuentaCorriente()
        { return ManejadorCuenta.getInstancia().listarCuentaCorriente(); }
        public List<Cuenta> listarTodas()
        {
            List<Cuenta> lstAux = ManejadorCuenta.getInstancia().listarTodos();
            lstAux.OrderBy(cuenta => cuenta.Numero);
            return lstAux;
        }
        #endregion
        #endregion
    }
}