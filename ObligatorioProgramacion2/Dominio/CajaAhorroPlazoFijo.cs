﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObligatorioProgramacion2.Dominio
{
    public class CajaAhorroPlazoFijo : Cuenta
    {
        #region //Atributos
        private DateTime _fechaInicio;
        private int _duracion;
        private decimal _porcentajeInteres;
        #endregion
        #region //Propiedades
        public DateTime FechaInicio
        {
            get { return _fechaInicio; }
            set { _fechaInicio = value; }
        }
        public int Duracion
        {
            get { return _duracion; }
            set { _duracion = value; }
        }
        public decimal PorcentajeInteres
        {
            get { return _porcentajeInteres; }
            set { _porcentajeInteres = value; }
        }
        #endregion
        #region //Metodos
        public CajaAhorroPlazoFijo() { }
        public CajaAhorroPlazoFijo(int pCiCliente, Moneda pMoneda, decimal pSaldoInicial, DateTime pFechaInicio, int pDuracion, decimal pPorcentajInteres)
            : base(pCiCliente, pMoneda, pSaldoInicial)
        {
            CiCliente = pCiCliente;
            MonedaCuenta = pMoneda;
            SaldoInicial = pSaldoInicial;
            Saldo = pSaldoInicial;
            _fechaInicio = pFechaInicio;
            _duracion = pDuracion;
            _porcentajeInteres = pPorcentajInteres;
        }
        public override string ToString()
        {
            return "CajaAhorroPLazoFijo " + base.ToString() + " Fecha de inicio: " + _fechaInicio + " Duracion: " + _duracion + " Porcentaje de interes: " + _porcentajeInteres + "%";
        }
        #endregion
    }
}